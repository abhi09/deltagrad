import read_data as rd
import util as util
import LinearRegression as LR
import torch
from torch.autograd import Variable
import matplotlib.pyplot as plt
import numpy as np
from numpy import savetxt
from numpy import asarray
import pdb

#Load data to work on
data = rd.load_data()

Y_train = data[util.target_column].to_numpy()
X_train = data.drop([util.target_column],axis=1).to_numpy()

model = LR.linearRegression(util.inputDim,util.outputDim).double()
if torch.cuda.is_available():
    model.cuda()

criterion = torch.nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(),lr=util.learningRate)

LOSS = []
weights = []
gradientWeights = []

for epoch in range(util.epochs):
    if torch.cuda.is_available():
        inputs = Variable(torch.from_numpy(X_train).cuda())
        labels = Variable(torch.from_numpy(Y_train).cuda())
    else:
        inputs = Variable(torch.from_numpy(X_train))
        labels = Variable(torch.from_numpy(Y_train))

    optimizer.zero_grad()
    outputs = model(inputs).view(-1)
    loss = criterion(outputs,labels)
    loss.backward()

    weight = model.linear.weight.detach().numpy()[0]
    bias = model.linear.bias.detach().numpy()
    combine = np.append(weight, bias, 0)
    weights.append(combine)

    weightGrad = model.linear.weight.grad.detach().numpy()[0]
    biasGrad = model.linear.bias.grad.detach().numpy()
    combineGrad = np.append(weightGrad, biasGrad, 0)
    gradientWeights.append(combineGrad)

    optimizer.step()
    if epoch < util.burnInPeriod:
        continue
    LOSS.append(loss.item())

savetxt(util.weightFileName,asarray(weights),delimiter=',')
savetxt(util.weightGradFileName,asarray(gradientWeights),delimiter=',')
plt.plot(LOSS)
plt.show()