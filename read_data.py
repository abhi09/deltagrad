#This file takes care of loading data and returning a pandas dataframe

import util as util
import pandas as pd

def load_data():
    data = pd.read_csv("{}/{}".format(util.data_directory, util.data_file));
    return data