import numpy as np

class NewtonOptimizations:

    def get_row(self,s,y):
        return np.linalg.inv(np.matmul(y.T,s))

    def BFGSMultiply(self,hessian_inverse, point_difference, gradient_difference, direction):
        r  = direction
        alpha = {}
        row = {}
        for i in range(len(point_difference),0,-1):
            row[i] = self.get_row(point_difference[i],gradient_difference[i])
            alpha[i] = np.matmul(row[i],point_difference[i].T,r)
            r = r - np.matmul(alpha[i],gradient_difference[i])

        r = np.matmul(hessian_inverse,r)

        for i in range(1, len(point_difference) + 1):
            beta = np.matmul(row[i],gradient_difference[i].T,r)
            r = r + np.matmul(alpha[len(point_difference) - i + 1] - beta,point_difference[i])

        return r